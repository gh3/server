import { AllowedOrigins } from '../Constants'

export const enableCORS = (req: any, res: any, next: Function) => {
  if (AllowedOrigins.includes(req.headers.host)) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With')
  }
  return next()
}
