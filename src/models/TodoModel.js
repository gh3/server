import mongoose, { Schema } from 'mongoose'
import mongooseStringQuery from 'mongoose-string-query'
import timestamps from 'mongoose-timestamp'

const TodoSchema = new Schema(
  {
    task: {
      type: String,
      required: true,
      trim: true
    },
    status: {
      type: String,
      required: true,
      enum: ['pending', 'complete', 'in progress', 'overdue'],
      default: 'pending'
    }
  },
  {
    minimize: false,
    collection: 'todos'
  },
)

TodoSchema.plugin(timestamps)
TodoSchema.plugin(mongooseStringQuery)

export default mongoose.model('TodoModel', TodoSchema)
