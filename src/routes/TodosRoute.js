import { InvalidContentError, InternalError, NotFoundError } from 'restify-errors'
import TodoModel from '../models/TodoModel'
import type { ServerType } from '../types/RestifyTypes'

export default (server: ServerType) => {
  server.post('/todos', async (req, res, next) => {
    if (!req.is('application/json')) {
      return next(new InvalidContentError('Expects application/json'))
    }

    if (!req.body) {
      return next(new InvalidContentError('Missing JSON'))
    }

    if (!req.body.task) {
      return next(new InvalidContentError('Task is required'))
    }

    let todo = new TodoModel(req.body)

    try {
      todo = await todo.save()
      res.send(todo)
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })

  server.get('/todos', async (req, res, next) => {
    try {
      const todos = await TodoModel.find()
      res.send(todos)
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })

  server.get('/todos/:todo_id', async (req, res, next) => {
    try {
      const todo = await TodoModel.findOne({ _id: req.params.todo_id })
      if (!todo) {
        return next(new NotFoundError('Task not found'))
      }
      res.send(todo)
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })

  server.put('/todos/:todo_id', async (req, res, next) => {
    if (!req.is('application/json')) {
      return next(new InvalidContentError('Expects application/json'))
    }

    if (!req.body) {
      return next(new InvalidContentError('Missing JSON'))
    }

    if (!req.body.task) {
      return next(new InvalidContentError('Task is required'))
    }

    try {
      const todo = await TodoModel.findOneAndUpdate({ _id: req.params.todo_id }, req.body, { new: true })
      if (!todo) {
        return next(new NotFoundError('Task not found'))
      }
      res.send(todo)
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })

  server.del('/todos/:todo_id', async (req, res, next) => {
    try {
      const status = await TodoModel.remove({ _id: req.params.todo_id })
      if (!status.n) {
        return next(new NotFoundError('Task not found'))
      }
      if (!status.ok) {
        return next(new InvalidContentError('Deleting failed'))
      }
      res.send({ _id: req.params.todo_id })
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })
}
