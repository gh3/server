import TodosRoute from './TodosRoute'
import MiscRoute from './MiscRoute'
import MinioRoute from './MinioRoute'
import type { ServerType } from '../types/RestifyTypes'

export default (server: ServerType) => {
  TodosRoute(server)
  MiscRoute(server)
  MinioRoute(server)
}
