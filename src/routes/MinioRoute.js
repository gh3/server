import type { ServerType } from '../types/RestifyTypes'
import { Client } from 'minio'
import { InternalError, InvalidContentError } from 'restify-errors'
import { MinioConfig } from '../Constants'
const MinioCliet = new Client(MinioConfig)

export default (server: ServerType) => {
  server.get('/minio/presigned-url/:bucket_name/:file_name', async (req, res, next) => {
    if (!req.params.bucket_name) {
      return next(new InvalidContentError('Bucket name is required'))
    }

    if (!req.params.file_name) {
      return next(new InvalidContentError('File name is required'))
    }

    try {
      const url = await MinioCliet.presignedPutObject(req.params.bucket_name, req.params.file_name)
      res.send({ url: url })
      next()
    } catch (err) {
      return next(new InternalError(err.message))
    }
  })
}
