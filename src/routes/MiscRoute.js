import type { ServerType } from '../types/RestifyTypes'

export default (server: ServerType) => {
  server.get('/', (req, res, next) => {
    res.send({
      gh3: 'ok',
      node_env: process.env.NODE_ENV
    })
    return next()
  })

  server.get('/misc', (req, res, next) => {
    res.send(200)
    return next()
  })
}
