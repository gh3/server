import restify, { plugins } from 'restify'
import { MongoConfig } from './Constants'
import mongoose from 'mongoose'
import routes from './routes'
import type { ServerType } from './types/RestifyTypes'
import { enableCORS } from './utils/ServerUtils'

const server: ServerType = restify.createServer()

server.use(plugins.jsonBodyParser())
server.use(plugins.acceptParser(server.acceptable))
server.use(enableCORS)

server.listen(8080, () => {
  mongoose.Promise = global.Promise
  mongoose.connect(MongoConfig.uri)

  const db = mongoose.connection

  db.on('error', (err) => {
    // eslint-disable-next-line no-console
    console.error(err)
    process.exit(1)
  })

  db.once('open', () => {
    routes(server)
    // eslint-disable-next-line no-console
    console.log(`Server is listening on port ${server.url}`)
  })
})
