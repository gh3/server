export type ServerType = {
  use: (Function) => Object,
  listen: (number, Function) => void,
  url: string,
  acceptable: Array<string>,
  get: (string, Function) => void,
  post: (string, Function) => void,
  put: (string, Function) => void,
  del: (string, Function) => void
}
