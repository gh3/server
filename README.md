# Development

This project was bootstrapped with [Example Node Server w/ Babel](https://github.com/babel/example-node-server).

After cloning repository you need to get all needed packages:

```sh
npm install
```

In the project directory, you can run:

```sh
npm run start
```

It runs the app in the development mode.
Open [http://localhost:8080](http://localhost:8080) to view running server.

Nodemon takes care that process restarts itself when you make edits in code. Keep in mind that babel compiles files on the fly and is not suitable for production. Babel options are saved `.babelrc` file.

## Flow

[Flow](https://flow.org/) is a static type checker for JavaScript. Maybe you will find this [flow-node-boilerplate](https://github.com/asci/flow-node-boilerplate) interesting.

Validate types in your code:

```sh
npm run flow
```

It checks entire project expect ignored folders/files. Take a look at the `.flowconfig` file to see what is ignored.

To verify current flow coverage percent run:

```sh
npm run flow:coverage
```

It prints coverage table into your console. More use friendly version does output into `flow-coverage` folder.

Covering takes into account only `src` folder. More settings regarding the flow coverage you can find in `.flowcoverage` file.

Building flow:

```sh
npm run flow:build
```

It actually doesn't build anything. It just removes type defintions from source files and output files into `src-clean` folder.

Flow doesn't know dependencies types out of the box. We need to install them separately:

```sh
npm run flow:deps
```

It saves libdefs into `flow-typed` folder. Not having them will probably cause type checking fail.

## ESLint

[ESLint](https://eslint.org/) is pluggable linting utility for JavaScript. Its configuration and rules are located in `.eslintrc` file.

To run ESLint use the following command:

```sh
npm run lint
```

It will perform eslint checks on all files inside `src` and `test` folder.

Some of the warnings are potentially auto fixable with:

```sh
npm run lint:fix
```

## Testing

```sh
npm run test
```

Launches the test runner in the interactive watch mode.

# Production

We are getting there.

## Build

To precompile source files into a production ready build run:

```sh
npm run build
```

Files are located in the `build` folder.

To serve this files run:

```sh
npm run serve
```

## Deploy

At current stage we use [zeit now](https://zeit.co/now) service for serving api. To deploy code (all files in this repo) run:

```sh
now
```

It will use `now-build` and `now-start` scripts from `package.json`. When server will be ready you will see unique URL in you console. Go test it, it should work already.

Last step is to set up alias:

```sh
now alias https://server-[unique_hash].now.sh https://gh3.now.sh
```

Server is now ready!
